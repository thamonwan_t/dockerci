package main

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

func sum(c echo.Context) error {
	x := c.FormValue("x")
	y := c.FormValue("y")

	xValue, yValue := cconvertXY(x, y)
	sumValue := sumXY(xValue, yValue)

	sumString := strconv.Itoa(sumValue)
	return c.String(http.StatusOK, sumString)
}

func cconvertXY(x string, y string) (int, int) {
	xValue, err := strconv.Atoi(x)
	if err != nil {
		return 0, 0
	}

	yValue, err := strconv.Atoi(y)
	if err != nil {
		return xValue, 0
	}

	return xValue, yValue
}

func sumXY(x int, y int) int {
	return x + y
}

package main

import (
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo"
)

func indexGet(c echo.Context) error {
	return c.Render(http.StatusOK, "input", nil)
}

func indexPost(c echo.Context) error {
	x := c.FormValue("x")
	y := c.FormValue("y")
	value := callBackEndSUM(x, y)
	return c.Render(http.StatusOK, "result", value)
}

func callBackEndSUM(x, y string) string {
	request, err := http.NewRequest("GET", "http://backend:5000/sum?x="+x+"&y="+y, nil)
	if err != nil {
		return "0"
	}

	res, err := http.DefaultClient.Do(request)
	if err != nil {
		return "0"
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "0"
	}

	return string(body)
}
